import React, { useState } from "react";
import { Box, Button, TextField } from "@mui/material";
import { Add } from "@mui/icons-material";

function CreateComp({ entityName, createFunction }) {
  const [formOpen, setFormOpen] = useState(false);
  const [inputValue, setInputValue] = useState("");

  function handleSubmit(event) {
    event.preventDefault();

    const title = inputValue.trim();
    setInputValue("");

    if (title) {
      createFunction(title);
    }
  }

  return (
    <>
      <Button
        onClick={() => setFormOpen(true)}
        variant="outlined"
        size="small"
        sx={{
          height: "32px",
          width: "90%",
          padding: "12px",
          backgroundColor: "rgba(239, 235, 235, 0.1)",
          color: "black",
          borderColor: "black",
          fontSize: "14px",
          textTransform: "none",
          border: "none",
          display: formOpen && "none",
          borderRadius: "8px",
        }}
      >
        <Add /> Add a {entityName}
      </Button>
      <Box
        component="form"
        onSubmit={handleSubmit}
        sx={{ display: !formOpen ? "none" : "flex", flexDirection: "column" }}
      >
        <TextField
          autoFocus
          required
          label={`Enter ${entityName} title...`}
          size="small"
          variant="outlined"
          sx={{ width: "100%" }}
          value={inputValue}
          onChange={(e) => setInputValue(e.target.value)}
        />
        <Box sx={{ display: "flex", marginTop: "10px" }}>
          <Button
            type="submit"
            variant="contained"
            size="small"
            sx={{ marginRight: "5px" }}
          >
            Add {entityName}
          </Button>
          <Button
            type="cancel"
            variant="filled"
            size="small"
            onClick={() => {
              setFormOpen(false);
              setInputValue("");
            }}
          >
            X
          </Button>
        </Box>
      </Box>
    </>
  );
}

export default CreateComp;
