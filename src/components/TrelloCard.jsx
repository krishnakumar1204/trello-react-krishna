import React, { useState } from "react";
import { Box, Button, IconButton } from "@mui/material";
import { Delete } from "@mui/icons-material";
import CardDetails from "./CardDetails";

function TrelloCard({ card, deleteTheCard }) {
  const [open, setOpen] = useState(false);

  return (
    <>
      <Box
        component="div"
        sx={{
          display: "flex",
          alignItems: "center",
          position: "relative",
          "&:hover .delete-icon": {
            display: "flex",
          },
        }}
      >
        <Button
          onClick={() => setOpen(true)}
          variant="outlined"
          size="small"
          sx={{
            width: "100%",
            display: "flex",
            justifyContent: "space-between",
            margin: "5px 0",
            color: "black",
            borderColor: "black",
            border: "1px solid white",
            backgroundColor: "white",
            textTransform: "none",
            borderRadius: "7px",
          }}
        >
          {card.name}{" "}
        </Button>
        <IconButton
          className="delete-icon"
          onClick={() =>  deleteTheCard(card.id)}
          sx={{ display: "none", position: "absolute", right: 0 }}
        >
          <Delete sx={{ height: "16px" }} />
        </IconButton>
      </Box>
      <CardDetails open={open} setOpen={setOpen} card={card} />
    </>
  );
}

export default TrelloCard;
