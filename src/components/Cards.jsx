import React, { useEffect, useState } from "react";
import { CardActions, CardContent } from "@mui/material";
import TrelloCard from "./TrelloCard";
import CreateCard from "./CreateCard";
import Notification from "./Notification";
import getAllCards from "../apis/getAllCards";
import createACard from "../apis/createACard";
import deleteCard from "../apis/deleteCard";

function Cards({ listId }) {
  const [cards, setCards] = useState([]);
  const [message, setMessage] = useState({ type: null, text: null });

  useEffect(() => {
    async function getData() {
      try {
        const data = await getAllCards(listId);
        setCards(data);
      } catch (error) {
        setMessage({
          type: "error",
          text: "Cards fetch failed! Please reload and retry!",
        });
        console.error(error);
      }
    }

    getData();
  }, []);

  async function createNewCard(cardTitle) {
    try {
      const newCard = await createACard(listId, cardTitle);
      setCards((prevCards) => [...prevCards, newCard]);
      setMessage({ type: "success", text: "New Card Added" });
    } catch (error) {
      setMessage({
        type: "error",
        message: "Card creation failed! Please reload and retry!",
      });
      console.error(error);
    }
  }

  async function deleteTheCard(cardId) {
    try {
      setCards((prevCards) => prevCards.filter((card) => card.id !== cardId));
      setMessage({ type: "success", text: "A Card deleted!" });
      await deleteCard(cardId);
    } catch (error) {
      setMessage({
        type: "error",
        text: "Card deletion failed! Please reload and retry!",
      });
      console.error(error);
    }
  }

  function handleSnackbarClose() {
    setMessage({ type: null, text: null });
  }

  return (
    <>
      {message.text && (
        <Notification message={message} handleClose={handleSnackbarClose} />
      )}
      <CardContent
        sx={{
          display: cards.length === 0 && "none",
          padding: "0 10px",
        }}
      >
        {cards.length > 0 &&
          cards.map((card) => {
            return (
              <TrelloCard
                key={card.id}
                card={card}
                deleteTheCard={deleteTheCard}
              />
            );
          })}
      </CardContent>
      <CardActions
        sx={{
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          padding: "8px 0",
        }}
      >
        <CreateCard createNewCard={createNewCard} />
      </CardActions>
    </>
  );
}

export default Cards;
