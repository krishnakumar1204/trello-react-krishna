import { Box, IconButton, ListItem, ListItemButton } from "@mui/material";
import React from "react";
import blueBackground from "../assets/blue-background.jpg";
import { Delete } from "@mui/icons-material";
import { Link } from "react-router-dom";

function SideBoard({ activeBoardId, board, deleteTheBoard }) {

  return (
    <ListItem
      disablePadding
      sx={{
        backgroundColor:
          activeBoardId === board.id
            ? "rgba(9, 30, 66, 0.1)"
            : "transparent",
        position: "relative",
        "&:hover .more-icon": {
          display: "flex",
        },
      }}
    >
      <ListItemButton component={Link} to={`/boards/${board.id}`}>
        <Box
          component="img"
          src={board.prefs.backgroundImage || blueBackground}
          sx={{
            width: "28px",
            height: "20px",
            marginRight: "5px",
            borderRadius: "2px",
          }}
        />
        {board.name}
      </ListItemButton>
      <Box
        className="more-icon"
        sx={{
          display: "none",
          position: "absolute",
          right: 0,
        }}
      >
        <IconButton onClick={() => deleteTheBoard(board.id)}>
          <Delete fontSize="small" />
        </IconButton>
      </Box>
    </ListItem>
  );
}

export default SideBoard;
