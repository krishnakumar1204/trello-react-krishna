import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { Box, Typography } from "@mui/material";
import Sidebar from "./Sidebar";
import List from "./List";
import CreateList from "./CreateList";
import Notification from "./Notification";
import Loading from "./Loading";
import getAllLists from "../apis/getAllLists";
import getBoardData from "../apis/getBoardData";
import createAList from "../apis/createAList";
import deleteList from "../apis/deleteList";

function BoardDetails() {
  const [lists, setLists] = useState([]);
  const [board, setBoard] = useState(null);
  const [message, setMessage] = useState({ type: null, text: null });
  const [loading, setLoading] = useState(true);

  const params = useParams();
  const boardId = params.boardId;

  useEffect(() => {
    async function getData() {
      try {
        const lData = await getAllLists(boardId);
        setLists(lData);
        const bData = await getBoardData(boardId);
        setBoard(bData);
        setLoading(false);
      } catch (error) {
        setLoading(false);
        setMessage({
          type: "error",
          text: "Lists Fetch failed! Please reload the page!",
        });
        console.error(error);
      }
    }

    getData();

    if (board) {
      document.title = `${board.name} | Trello`;
    }
  }, [boardId]);

  async function createNewList(listTitle) {
    try {
      const newList = await createAList(listTitle, boardId);
      setLists((prevLists) => [newList, ...prevLists]);
      setMessage({ type: "success", text: "New list added!" });
    } catch (error) {
      setMessage({
        type: "error",
        message: "List creation failed! Please reload and retry!",
      });
      console.error(error);
    }
  }

  async function deleteTheList(listId) {
    try {
      setLists((prevLists) => prevLists.filter((list) => list.id !== listId));
      setMessage({ type: "success", text: "A List deleted!" });
      await deleteList(listId);
    } catch (error) {
      setMessage({
        type: "error",
        text: "List deletion failed! Please reload and retry!",
      });
      console.error(error);
    }
  }

  function handleSnackbarClose() {
    setMessage({ type: null, text: null });
  }

  return (
    <>
    
      {message.text && (
        <Notification message={message} handleClose={handleSnackbarClose} />
      )}
      <Box
        sx={{
          backgroundImage:
            board && board.prefs && `url(${board.prefs.backgroundImage})`,
          backgroundSize: "cover",
          backgroundPosition: "center",
        }}
      >
        <Box>
          <Sidebar activeBoardId={boardId} />
        </Box>
        <Box sx={{ padding: "70px 0 0 19vw" }}>
          <Typography sx={{ fontSize: "20px", fontWeight: "bold" }}>
            {board ? board.name : ""}
          </Typography>
        </Box>
        <Box
          component="section"
          sx={{
            display: "-webkit-inline-box",
            columnGap: "15px",
            width: "83vw",
            height: "calc(100vh - 110px)",
            margin: "10px 0 0 17vw",
            paddingLeft: "15px",
            alignItems: "flex-start",
            overflowX: "auto",
          }}
        >
          {loading && <Loading />}
          {lists.length > 0 &&
            lists.map((list) => {
              return (
                <List key={list.id} list={list} deleteTheList={deleteTheList} />
              );
            })}
          <CreateList createNewList={createNewList} />
        </Box>
      </Box>
    </>
  );
}

export default BoardDetails;
