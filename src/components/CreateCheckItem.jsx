import React from "react";
import { Box } from "@mui/material";
import CreateComp from "./CreateComp";

function CreateCheckItem({ checklistId, createNewCheckitem }) {
  
  function createCheckitem(checkitemTitle) {
    createNewCheckitem(checklistId, checkitemTitle);
  }

  return (
    <Box sx={{ width: "92%", margin: "0 0 30px 30px" }}>
      <CreateComp entityName="Checkitem" createFunction={createCheckitem} />
    </Box>
  );
}

export default CreateCheckItem;
