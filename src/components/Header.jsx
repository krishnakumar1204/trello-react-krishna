import { AppBar, Box, IconButton, TextField, Toolbar } from "@mui/material";
import React from "react";
import TrelloLogo from "../assets/Trello-logo2.png";
import { Link } from "react-router-dom";

function Header() {
  return (
    <Box>
      <AppBar
        sx={{
          backgroundColor: "white",
          display: "flex",
          flexDirection: "row",
          height: "48px",
          alignItems: "center",
          justifyContent: "space-between",
          padding: "0 40px",
          background: "rgba(255, 255, 255, 0.9)",
        }}
      >
        <Link to="/boards" sx={{ color: "black", borderRadius: "2px" }}>
          <Box
            component="img"
            src={TrelloLogo}
            alt="Trello-Logo"
            sx={{ height: "17px" }}
          />
        </Link>
        <Box>
          <TextField
            variant="outlined"
            size="small"
            label="Search"
            InputLabelProps={{
              style: {
                fontSize: '14px'
              },
            }}
            inputProps={{
              style: {
                padding: "5px 14px",
              },
            }}
          />
        </Box>
      </AppBar>
    </Box>
  );
}

export default Header;
