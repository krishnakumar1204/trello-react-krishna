import React, { useState } from "react";
import Snackbar from "@mui/material/Snackbar";
import Alert from "@mui/material/Alert";

export default function Notification({message, handleClose}) {

  return (
    <div>
      <Snackbar 
      open={message.text !== null} 
      autoHideDuration={1500} 
      onClose={handleClose}
      >
        <Alert
          onClose={handleClose}
          severity={message.type}
          variant="filled"
          sx={{ width: "100%", }}
        >
          {message.text}
        </Alert>
      </Snackbar>
    </div>
  );
}
