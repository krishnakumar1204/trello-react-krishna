import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  TextField,
} from "@mui/material";

function CreateModal({ entityName, modalOpen, setModelOpen, createFunction}) {

  function handleSubmit(event) {
    event.preventDefault();
    setModelOpen(false);
    const formData = new FormData(event.currentTarget);
    const formJson = Object.fromEntries(formData.entries());
    const title = formJson.title.trim();
    if(title){
        createFunction(title);
    }
  }

  return (
    <Dialog
      open={modalOpen}
      onClose={() => setModelOpen(false)}
      component="form"
      onSubmit={handleSubmit}
    >
      <DialogTitle sx={{ fontSize: "16px", fontWeight: "bold" }}>
        Create {entityName}
      </DialogTitle>
      <DialogContent>
        <TextField
          autoFocus
          required
          name="title"
          label={`${entityName} Title`}
          type="text"
          variant="standard"
          size="small"
          InputLabelProps={{
            style: {
              fontSize: "14px",
            },
          }}
        />
      </DialogContent>
      <DialogActions>
        <Button
          onClick={() => setModelOpen(false)}
          size="small"
          sx={{
            color: "black",
            "&:hover": {
              backgroundColor: "rgba(9, 30, 66, 0.04)",
            },
          }}
        >
          Cancel
        </Button>
        <Button
          size="small"
          type="submit"
          sx={{
            backgroundColor: "rgba(9, 30, 66, 0.06)",
            color: "black",
            "&:hover": {
              backgroundColor: "rgba(9, 30, 66, 0.15)",
            },
          }}
        >
          Create
        </Button>
      </DialogActions>
    </Dialog>
  );
}

export default CreateModal;
