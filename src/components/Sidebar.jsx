import { Box, Drawer, List, Typography } from "@mui/material";
import React, { useEffect, useState } from "react";
import getAllBoards from "../apis/getAllBoards";
import SideBoard from "./SideBoard";
import deleteBoard from "../apis/deleteBoard";
import { useNavigate } from "react-router-dom";
import Notification from "./Notification";

function Sidebar({ activeBoardId }) {
  const [boards, setBoards] = useState([]);
  const [message, setMessage] = useState({type: null, text: null});

  const navigate = useNavigate();

  useEffect(() => {
    async function getData() {
      try {
        const data = await getAllBoards();
        setBoards(data);
      } catch (error) {
        setMessage({type: "error", text: "Boards fetch failed! Please reload and retry!"});
        console.error(error);
      }
    }

    getData();
  }, []);

  async function deleteTheBoard(boardId) {
    try {
      setBoards((prevBoards) =>
        prevBoards.filter((board) => board.id !== boardId)
      );
      setMessage({type: "success", text: "A Board deleted!"});
      await deleteBoard(boardId);
      if (boardId === activeBoardId) {
        navigate(-1);
      }
    } catch (error) {
      setMessage({type: "error", text: "Board deletion failed! Please reload and retry!"});
      console.error(error);
    }
  }

  function handleSnackbarClose() {
    setMessage({ type: null, text: null });
  }

  return (
    <>
      {message.text && (
        <Notification message={message} handleClose={handleSnackbarClose} />
      )}
      <Drawer
        variant="permanent"
        sx={{
          flexShrink: 0,
          [`& .MuiDrawer-paper`]: {
            width: "17vw",
            boxSizing: "border-box",
            height: "calc(100% - 48px)",
            marginTop: "48px",
            background: "rgba(255, 255, 255, 0.9)",
          },
        }}
      >
        <Box sx={{ overflow: "auto" }}>
          <Typography sx={{ fontWeight: "bold", padding: " 8px 16px" }}>
            Your Boards
          </Typography>
          <List>
            {boards &&
              boards.map((board) => (
                <SideBoard
                  key={board.id}
                  activeBoardId={activeBoardId}
                  board={board}
                  deleteTheBoard={deleteTheBoard}
                />
              ))}
          </List>
        </Box>
      </Drawer>
    </>
  );
}

export default Sidebar;
