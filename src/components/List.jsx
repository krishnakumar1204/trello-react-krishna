import React from "react";
import { Card, CardHeader, Typography } from "@mui/material";
import Cards from "./Cards";
import DeleteButton from "./DeleteButton";

function List({ list, deleteTheList }) {

  function deleteList() {
    deleteTheList(list.id);
  }

  return (
    <>
      <Card
        sx={{
          width: "272px",
          padding: "0",
          height: "fit-content",
          backgroundColor: "rgb(241, 242, 244)",
          borderRadius: "10px",
        }}
      >
        <CardHeader
          sx={{ padding: "8px 15px" }}
          action={<DeleteButton handleFunction={deleteList} />}
          title={
            <Typography sx={{ fontSize: "14px", fontWeight: "bold" }}>
              {list.name}
            </Typography>
          }
        />
        <Cards listId={list.id} />
      </Card>
    </>
  );
}

export default List;
