import React from "react";
import { Box, Button, Typography } from "@mui/material";
import { IoMdCheckboxOutline } from "react-icons/io";
import LinearProgressWithLabel from "./LinearProgressWithLabel";
import CheckItem from "./CheckItem";
import CreateCheckItem from "./CreateCheckItem";

function Checklist({
  checklist,
  deleteTheChecklist,
  createNewCheckitem,
  deleteTheCheckitem,
  updateTheCheckitem,
}) {
  const checkitems = checklist.checkItems;

  function getProgress(checkitems) {
    let total = checkitems.length;
    if (total === 0) {
      return 0;
    }

    let cnt = 0;
    for (let i = 0; i < checkitems.length; i++) {
      if (checkitems[i].state === "complete") {
        cnt++;
      }
    }

    return Math.round((cnt / total) * 100);
  }

  return (
    <>
      <Box
        sx={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
          width: "492px",
        }}
      >
        <Typography
          sx={{
            display: "flex",
            alignItems: "center",
            columnGap: "25px",
            fontWeight: "bold",
          }}
        >
          <IoMdCheckboxOutline fontSize="large" />
          {checklist.name}
        </Typography>
        <Button
          onClick={() => deleteTheChecklist(checklist.id)}
          variant="contained"
          size="small"
          sx={{
            backgroundColor: "rgba(9, 30, 66, 0.06)",
            color: "black",
            "&:hover": { backgroundColor: "rgba(9, 30, 66, 0.1)" },
          }}
        >
          Delete
        </Button>
      </Box>
      <Box sx={{ width: "100%", margin: "5px 0" }}>
        <LinearProgressWithLabel
          value={checkitems && getProgress(checkitems)}
        />
      </Box>
      <Box
        component="div"
        sx={{ display: "flex", flexDirection: "column", width: "100%" }}
      >
        {checkitems.length > 0 &&
          checkitems.map((checkitem) => {
            return (
              <CheckItem
                key={checkitem.id}
                checkitem={checkitem}
                deleteTheCheckitem={deleteTheCheckitem}
                updateTheCheckitem={updateTheCheckitem}
              />
            );
          })}
        <CreateCheckItem
          checklistId={checklist.id}
          createNewCheckitem={createNewCheckitem}
        />
      </Box>
    </>
  );
}

export default Checklist;
