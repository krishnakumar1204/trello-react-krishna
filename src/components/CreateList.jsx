import React, { useState } from "react";
import { Box } from "@mui/material";
import CreateComp from "./CreateComp";

function CreateList({ createNewList }) {

  return (
    <Box
      sx={{
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        padding: "8px 0",
        width: "272px",
        height: "fit-content",
        backgroundColor: "rgb(241, 242, 244)",
        borderRadius: "10px",
      }}
    >
      <CreateComp
        entityName="List"
        createFunction={createNewList}
      />
    </Box>
  );
}

export default CreateList;
