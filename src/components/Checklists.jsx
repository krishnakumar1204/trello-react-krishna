import React, { useEffect, useState } from "react";
import { Box } from "@mui/material";
import Checklist from "./Checklist";
import CreateChecklist from "./CreateChecklist";
import Notification from "./Notification";
import Loading from "./Loading";
import getAllChecklists from "../apis/getAllChecklists";
import createChecklist from "../apis/createChecklist";
import deleteChecklist from "../apis/deleteChecklist";
import createCheckItem from "../apis/createCheckItem";
import deleteCheckItem from "../apis/deleteCheckItem";
import updateCheckItem from "../apis/updateCheckItem";

function Checklists({ cardId }) {
  const [checklists, setChecklists] = useState([]);
  const [message, setMessage] = useState({ type: null, text: null });
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    async function getData() {
      try {
        const data = await getAllChecklists(cardId);
        setChecklists(data);
        setLoading(false);
      } catch (error) {
        setMessage({
          type: "error",
          text: "Checklists fetch failed! Please reload and retry!",
        });
        console.error(error);
      }
    }
    getData();
  }, []);

  async function createNewChecklist(checklistTitle) {
    try {
      const newChecklist = await createChecklist(cardId, checklistTitle);
      setChecklists((prevChecklists) => [...prevChecklists, newChecklist]);
      setMessage({ type: "success", text: "New Checklist created!" });
    } catch (error) {
      setMessage({
        type: "error",
        text: "Checklist creation failed! Please reload and retry!",
      });
      console.error(error);
    }
  }

  async function deleteTheChecklist(checklistId) {
    try {
      setChecklists((prevChecklists) =>
        prevChecklists.filter((checklist) => checklist.id !== checklistId)
      );
      setMessage({ type: "success", text: "A Checklist deleted!" });
      await deleteChecklist(checklistId);
    } catch (error) {
      setMessage({
        type: "error",
        text: "Checklist deletion failed! Please reload and retry!",
      });
      console.error(error);
    }
  }

  async function createNewCheckitem(checklistId, checkitemTitle) {
    try {
      const newCheckitem = await createCheckItem(checklistId, checkitemTitle);
      setChecklists((prevChecklists) =>
        prevChecklists.map((checklist) => {
          if (checklist.id === checklistId) {
            return {
              ...checklist,
              checkItems: [...checklist.checkItems, newCheckitem],
            };
          } else {
            return checklist;
          }
        })
      );
      setMessage({ type: "success", text: "New Checkitem added!" });
    } catch (error) {
      setMessage({
        type: "error",
        text: "Checkitem creation failed! Please reload and retry!",
      });
      console.error(error);
    }
  }

  async function deleteTheCheckitem(checkitem) {
    try {
      setChecklists((prevChecklists) =>
        prevChecklists.map((checklist) => {
          if (checklist.id === checkitem.idChecklist) {
            return {
              ...checklist,
              checkItems: checklist.checkItems.filter(
                (item) => item.id !== checkitem.id
              ),
            };
          } else {
            return checklist;
          }
        })
      );
      setMessage({ type: "success", text: "A Checkitem deleted!" });
      await deleteCheckItem(cardId, checkitem.id);
    } catch (error) {
      setMessage({
        type: "error",
        text: "Checkitem deletion failed! Please reload and retry!",
      });
      console.error(error);
    }
  }

  async function updateTheCheckitem(checkitem, checked) {
    try {
      setChecklists((prevChecklists) =>
        prevChecklists.map((checklist) => {
          if (checklist.id === checkitem.idChecklist) {
            return {
              ...checklist,
              checkItems: checklist.checkItems.map((item) => {
                if (item.id === checkitem.id) {
                  return {
                    ...item,
                    state: checked ? "incomplete" : "complete",
                  };
                }
                return item;
              }),
            };
          } else {
            return checklist;
          }
        })
      );

      setMessage({ type: "success", text: "Checkitem updated!" });

      await updateCheckItem(
        cardId,
        checkitem.id,
        checked ? "incomplete" : "complete"
      );
    } catch (error) {
      setMessage({
        type: "error",
        text: "Checkitem updation failed! Please reload and retry!",
      });
      console.error(error);
    }
  }

  function handleSnackbarClose() {
    setMessage({ type: null, text: null });
  }

  return (
    <>
    {loading && <Loading />}
      {message.text && (
        <Notification message={message} handleClose={handleSnackbarClose} />
      )}
      <Box sx={{ width: "500px", marginRight: "10px" }}>
        {checklists.length > 0 &&
          checklists.map((checklist) => {
            return (
              <Checklist
                key={checklist.id}
                checklist={checklist}
                deleteTheChecklist={deleteTheChecklist}
                createNewCheckitem={createNewCheckitem}
                deleteTheCheckitem={deleteTheCheckitem}
                updateTheCheckitem={updateTheCheckitem}
              />
            );
          })}
      </Box>
      <Box sx={{ width: "200px" }}>
        <CreateChecklist createNewChecklist={createNewChecklist} />
      </Box>
    </>
  );
}

export default Checklists;
