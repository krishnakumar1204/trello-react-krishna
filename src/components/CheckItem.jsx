import React, { useState } from "react";
import { Box, Checkbox, FormControlLabel, IconButton } from "@mui/material";
import { Delete } from "@mui/icons-material";

function CheckItem({ checkitem, deleteTheCheckitem, updateTheCheckitem }) {
  const [checked, setChecked] = useState(checkitem.state === "complete");

  async function handleChange() {
    setChecked(checked ? false : true);
    updateTheCheckitem(checkitem, checked);
  }

  return (
    <Box sx={{ display: "flex", justifyContent: "space-between" }}>
      <FormControlLabel
        control={<Checkbox checked={checked} onChange={handleChange} />}
        label={checkitem.name}
        sx={{
          "& .MuiFormControlLabel-label": {
            textDecoration: checked ? "line-through" : "none",
          },
          width: "100%",
        }}
      />
      <IconButton onClick={() => deleteTheCheckitem(checkitem)}>
        <Delete sx={{ width: "18px", height: "14px" }} />
      </IconButton>
    </Box>
  );
}

export default CheckItem;
