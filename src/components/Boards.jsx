import React, { useEffect, useState } from "react";
import { Box, Typography } from "@mui/material";
import Board from "./Board.jsx";
import CreateBoard from "./CreateBoard.jsx";
import Loading from "./Loading.jsx";
import Notification from "./Notification.jsx";
import getAllBoards from "../apis/getAllBoards.js";
import createABoard from "../apis/createABoard";

function Boards() {
  const [boards, setBoards] = useState([]);
  const [message, setMessage] = useState({ type: null, text: null });
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    document.title = "Boards | Trello";

    async function getData() {
      try {
        const data = await getAllBoards();
        setBoards(data);
        setLoading(false);
      } catch (error) {
        setLoading(false);
        setMessage({
          type: "error",
          text: "Boards fetch failed! Please reload the page!",
        });
        console.error(error.message);
      }
    }
    getData();
  }, []);

  async function createNewBoard(boardTitle) {
    try {
      const newBoard = await createABoard(boardTitle);
      setBoards((prevBoards) => [...prevBoards, newBoard]);
      setMessage({ type: "success", text: "New board created!" });
    } catch (error) {
      setMessage({
        type: "error",
        text: "Board creation failed! Please reload and retry!",
      });
      console.log(error.message);
    }
  }

  function handleSnackbarClose() {
    setMessage({ type: null, text: null });
  }

  return (
    <>
      {message.text && (
        <Notification message={message} handleClose={handleSnackbarClose} />
      )}

      <Box sx={{ margin: "110px 10vw 20px 30vw" }}>
        <Box>
          <Typography
            variant="h3"
            sx={{ fontSize: "16px", fontWeight: "bold" }}
          >
            YOUR WORKSPACES
          </Typography>
        </Box>

        <Box sx={{ display: "flex", alignItems: "center", margin: "20px 0" }}>
          <Box
            sx={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              width: "32px",
              height: "32px",
              fontSize: "24px",
              background: "linear-gradient(#0747a6, #008da6)",
              color: "white",
              borderRadius: "3px",
            }}
          >
            K
          </Box>
          <Typography
            variant="h3"
            sx={{ fontSize: "16px", fontWeight: "bold", marginLeft: "10px" }}
          >
            Krishna Kumar's workspace
          </Typography>
        </Box>

        <Box>
          {loading && <Loading />}
          {boards.length > 0 &&
            boards.map((board) => {
              return <Board key={board.id} board={board} />;
            })}

          <CreateBoard createNewBoard={createNewBoard} />
        </Box>
      </Box>
    </>
  );
}

export default Boards;
