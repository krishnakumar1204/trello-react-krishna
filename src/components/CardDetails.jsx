import React from "react";
import {
  Box,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
} from "@mui/material";
import Checklists from "./Checklists";

function CardDetails({ open, setOpen, card }) {
  return (
      <Dialog
        maxWidth={false}
        sx={{ width: "768px", maxWidth: "768px", justifySelf: "center" }}
        open={open}
        onClose={() => setOpen(false)}
      >
        <Box sx={{ display: "flex", justifyContent: "space-between" }}>
          <DialogTitle sx={{ fontWeight: "bold" }}>{card.name}</DialogTitle>
          <DialogActions>
            <Button
              onClick={() => setOpen(false)}
              sx={{ color: "black", fontSize: "20px" }}
            >
              X
            </Button>
          </DialogActions>
        </Box>
        <DialogContent sx={{ display: "flex" }}>
          <Checklists cardId={card.id} />
        </DialogContent>
      </Dialog>
  );
}

export default CardDetails;
