import React, { useState } from "react";
import { Button } from "@mui/material";
import CreateModal from "./CreateModal";

function CreateBoard({ createNewBoard }) {
  const [modalOpen, setModelOpen] = useState(false);

  return (
    <>
      <Button
        onClick={() => setModelOpen(true)}
        variant="contained"
        sx={{
          height: "96px",
          width: "194px",
          margin: "0px 20px 30px 0",
          padding: "0",
          fontSize: "14px",
          textTransform: "none",
          backgroundColor: "rgba(9, 30, 66, 0.06)",
          color: "black",
          "&:hover": {
            backgroundColor: "rgba(9, 30, 66, 0.1)",
          },
        }}
      >
        Create New Board
      </Button>

      <CreateModal
        entityName="Board"
        modalOpen={modalOpen}
        setModelOpen={setModelOpen}
        createFunction={createNewBoard}
      />
    </>
  );
}

export default CreateBoard;
