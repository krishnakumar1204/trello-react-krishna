import { Button } from "@mui/material";
import React from "react";

function Board({ board }) {
  return (
    <>
      <Button
        href={`/boards/${board.id}`}
        variant="contained"
        sx={{
          height: "96px",
          width: "194px",
          margin: "0 20px 30px 0",
          padding: "5px 0 0 8px",
          backgroundImage: `url(${board.prefs.backgroundImage})`,
          backgroundSize: "cover",
          backgroundPosition: "center",
          fontSize: "16px",
          fontWeight: "bold",
          textTransform: "none",
          justifyContent: "flex-start",
          alignItems: "flex-start",
        }}
      >
        {board.name}
      </Button>
    </>
  );
}

export default Board;
