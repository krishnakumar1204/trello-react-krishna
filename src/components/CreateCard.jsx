import React, { useState } from "react";
import CreateComp from "./CreateComp";

function CreateCard({ createNewCard }) {

  return (
    <>
      <CreateComp
        entityName="Card"
        createFunction={createNewCard}
      />
    </>
  );
}

export default CreateCard;
