import React from 'react';
import CircularProgress from '@mui/material/CircularProgress';
import Box from '@mui/material/Box';

export default function Loading() {
  return (
    <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: "100%", width: "100%" }}>
      <CircularProgress size={80} thickness={2} />
    </Box>
  );
}
