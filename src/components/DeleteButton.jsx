import * as React from 'react';
import Button from '@mui/material/Button';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import Fade from '@mui/material/Fade';
import { MoreHoriz } from '@mui/icons-material';

export default function DeleteButton({ handleFunction }) {
  
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <div>
      <Button
        id="fade-button"
        aria-controls={open ? 'fade-menu' : undefined}
        aria-haspopup="true"
        aria-expanded={open ? 'true' : undefined}
        onClick={handleClick}
        size='small'
        sx={{color: 'black', padding: '0', maxWidth: '30px', maxHeight: '20px', minWidth: '30px', minHeight: '25px'}}
      >
        <MoreHoriz fontSize='small' />
      </Button>
      <Menu
        id="fade-menu"
        MenuListProps={{
          'aria-labelledby': 'fade-button',
        }}
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
        TransitionComponent={Fade}
        sx={{".MuiMenu-list": {
            padding: "2px"
        }}}
      >
        <MenuItem onClick={handleFunction} sx={{padding: "0 10px", fontSize: "14px"}}>Delete</MenuItem>
      </Menu>
    </div>
  );
}
