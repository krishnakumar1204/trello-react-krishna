import { Box, LinearProgress, Typography } from "@mui/material";

export default function LinearProgressWithLabel({ value }) {
  return (
    <Box sx={{ display: "flex", alignItems: "center" }}>
      <Box sx={{ minWidth: 35, mr: 1 }}>
        <Typography variant="body2" color="text.secondary">
          {`${value}%`}
        </Typography>
      </Box>
      <Box sx={{ width: "100%" }}>
        <LinearProgress
          variant="determinate"
          color={value === 100 ? "success" : "primary"}
          value={value}
        />
      </Box>
    </Box>
  );
}
