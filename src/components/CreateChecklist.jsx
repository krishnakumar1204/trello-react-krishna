import React, { useState } from "react";
import { Button, Typography } from "@mui/material";
import { IoMdCheckboxOutline } from "react-icons/io";
import CreateModal from "./CreateModal";

function CreateChecklist({ createNewChecklist }) {
  const [modalOpen, setModelOpen] = useState(false);

  return (
    <>
      <Typography>Add to card</Typography>
      <Button
        onClick={() => setModelOpen(true)}
        startIcon={<IoMdCheckboxOutline />}
      >
        Checklist
      </Button>
      <CreateModal
        entityName="Checklist"
        modalOpen={modalOpen}
        setModelOpen={setModelOpen}
        createFunction={createNewChecklist}
      />
    </>
  );
}

export default CreateChecklist;
