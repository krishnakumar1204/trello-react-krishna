import { BrowserRouter, Routes, Route } from "react-router-dom";
import "./App.css";
import "./reset.css";
import Header from "./components/Header";
import Boards from "./components/Boards";
import BoardDetails from "./components/BoardDetails";

function App() {
  return (
    <BrowserRouter>
      <Header />
      <Routes>
        <Route path="/boards" element={<Boards />} />
        <Route path="/boards/:boardId" element={<BoardDetails />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
