import axios from 'axios';
import { APIKey, APIToken } from './secretData.js';

export default async function getAllBoards() {
  const response = await axios.get(`https://api.trello.com/1/members/me/boards?key=${APIKey}&token=${APIToken}`);
  return response.data;
}
