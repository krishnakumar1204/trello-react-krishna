import axios from "axios";
import { APIKey, APIToken } from "./secretData.js";


export default async function createChecklist(cardId, checklistTitle) {
    try {
        const response = await axios.post(`https://api.trello.com/1/cards/${cardId}/checklists?key=${APIKey}&token=${APIToken}&name=${checklistTitle}`)
        return response.data;
    }
    catch (error) {
        return new Error('An error occurred: ' + error.message);
    }
}