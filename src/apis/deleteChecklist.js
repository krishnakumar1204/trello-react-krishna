import axios from "axios";
import { APIKey, APIToken } from "./secretData.js";

export default async function deleteChecklist(checklistId) {
    try {
        await axios.delete(`https://api.trello.com/1/checklists/${checklistId}?key=${APIKey}&token=${APIToken}`);
    }
    catch (error) {
        return new Error('An error occurred: ' + error.message);
    }
}