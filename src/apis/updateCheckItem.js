import axios from "axios";
import { APIKey, APIToken } from "./secretData.js";

export default async function updateCheckItem(cardId, checkItemId, state) {
    try {
        await axios.put(`https://api.trello.com/1/cards/${cardId}/checkItem/${checkItemId}?key=${APIKey}&token=${APIToken}&state=${state}`)
        return response.data;
    }
    catch (error) {
        return new Error('An error occurred: ' + error.message);
    }
}