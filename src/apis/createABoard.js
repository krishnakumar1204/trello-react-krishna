import axios from "axios";
import { APIKey, APIToken } from "./secretData.js";

export default async function createABoard(boardTitle) {
  const response = await axios.post(
    `https://api.trello.com/1/boards/`, null, {
    params: {
      name: boardTitle,
      key: APIKey,
      token: APIToken,
    }
  });

  return response.data;
}
