import axios from "axios";
import { APIKey, APIToken } from "./secretData.js";

export default async function getAllLists(boardId) {
    try {
        const response = await axios.get(`https://api.trello.com/1/boards/${boardId}/lists?key=${APIKey}&token=${APIToken}`);
        return response.data;
    }
    catch (error) {
        return new Error('An error occurred: ' + error.message);
    }
}

