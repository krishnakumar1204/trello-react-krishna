import axios from "axios";
import { APIKey, APIToken } from "./secretData.js";

export default async function getAllCards(listId) {
    try {
        const response = await axios.get(`https://api.trello.com/1/lists/${listId}/cards?key=${APIKey}&token=${APIToken}`);
        return response.data;
    }
    catch (error) {
        return new Error('An error occurred: ' + error.message);
    }

}