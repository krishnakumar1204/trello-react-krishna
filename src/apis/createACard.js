import axios from "axios";
import { APIKey, APIToken } from "./secretData.js";


export default async function createACard(listId, cardName) {
    try {
        const response = await axios.post(`https://api.trello.com/1/cards?idList=${listId}&name=${cardName}&key=${APIKey}&token=${APIToken}`);
        return response.data;
    }
    catch (error) {
        return new Error('An error occurred: ' + error.message);
    }
}