import axios from "axios";
import { APIKey, APIToken } from "./secretData.js";

export default async function getAllCheckitems(checklistId) {
    try {
        const response = await axios.get(`https://api.trello.com/1/checklists/${checklistId}/checkItems?key=${APIKey}&token=${APIToken}`)
        return response.data;
    }
    catch (error) {
        return new Error('An error occurred: ' + error.message);
    }
}