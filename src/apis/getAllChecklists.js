import axios from "axios";
import { APIKey, APIToken } from "./secretData.js";


export default async function getAllChecklists(cardId) {
    try {
        const response = await axios.get(`https://api.trello.com/1/cards/${cardId}/checklists?key=${APIKey}&token=${APIToken}`)
        return response.data;
    }
    catch (error) {
        return new Error('An error occurred: ' + error.message);
    }
}