import axios from "axios";
import { APIKey, APIToken } from "./secretData.js";

export default async function deleteList(listId) {
    try {
        await axios.put(`https://api.trello.com/1/lists/${listId}?key=${APIKey}&token=${APIToken}&closed=true`);
    }
    catch (error) {
        return new Error('An error occurred: ' + error.message);
    }
}