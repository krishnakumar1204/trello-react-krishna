import axios from "axios";
import { APIKey, APIToken } from "./secretData.js";


export default async function createAList(listName, boardId) {
    try {
        const response = await axios.post(`https://api.trello.com/1/lists?name=${listName}&idBoard=${boardId}&key=${APIKey}&token=${APIToken}`);
        return response.data;
    }
    catch (error) {
        return new Error('An error occurred: ' + error.message);
    }
}