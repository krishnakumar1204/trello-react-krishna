import axios from "axios";
import { APIKey, APIToken } from "./secretData.js";

export default async function createCheckItem(checklistId, checkItemTitle) {
    try {
        const response = await axios.post(`https://api.trello.com/1/checklists/${checklistId}/checkItems?name=${checkItemTitle}&key=${APIKey}&token=${APIToken}`)
        return response.data;
    }
    catch(error){
        return new Error('An error occurred: ' + error.message);
    }
}