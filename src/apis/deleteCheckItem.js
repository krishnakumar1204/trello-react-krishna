import axios from "axios";
import { APIKey, APIToken } from "./secretData.js";

export default async function deleteCheckItem(cardId, checkItemId) {
    try {
        await axios.delete(`https://api.trello.com/1/cards/${cardId}/checkItem/${checkItemId}?key=${APIKey}&token=${APIToken}`);
    }
    catch (error) {
        return new Error('An error occurred: ' + error.message);
    }
}