// 1. Create a function getBoard which takes the boardId as arugment and returns a promise which resolves with board data
import axios from "axios";
import { APIKey, APIToken } from "./secretData.js";

export default async function getBoardData(boardId) {
  try {
    const response = await axios.get(`https://api.trello.com/1/boards/${boardId}?key=${APIKey}&token=${APIToken}`);
    return response.data;
  }
  catch (error) {
    return new Error('An error occurred: ' + error.message);
  }
}
