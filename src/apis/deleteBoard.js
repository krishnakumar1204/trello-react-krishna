import axios from "axios";
import { APIKey, APIToken } from "./secretData.js";

export default async function deleteBoard(boardId) {
    try {
        await axios.delete(`https://api.trello.com/1/boards/${boardId}?key=${APIKey}&token=${APIToken}`);
    }
    catch (error) {
        return new Error('An error occurred: ' + error.message);
    }
}