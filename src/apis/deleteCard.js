import axios from "axios";
import { APIKey, APIToken } from "./secretData.js";

export default async function deleteCard(cardId) {
    try {
        await axios.delete(`https://api.trello.com/1/cards/${cardId}?key=${APIKey}&token=${APIToken}`);
    }
    catch (error) {
        return new Error('An error occurred: ' + error.message);
    }
}